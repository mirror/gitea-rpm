%global   gitea_user  git

Name:           gitea
Version:        1.11.6
Release:        1%{?dist}
Summary:        Git with a cup of tea, painless self-hosted git service
BuildArch:      x86_64 aarch64

Group:          System Environment/Daemons
License:        MIT
URL:            https://gitea.io/
Source0:        https://github.com/go-gitea/gitea/releases/download/v%{version}/%{name}-src-%{version}.tar.gz
Source1:        gitea.service
Source2:        app.ini.sample
Source3:        LICENSE

BuildRequires:  systemd-units
BuildRequires:  golang >= 1.11.0
BuildRequires:  pam-devel
BuildRequires:  nodejs >= 10.0.0
BuildRequires:  npm

Requires(pre):  shadow-utils
Requires:       systemd glibc git

%description
Gitea is a community managed fork of Gogs, lightweight code hosting solution
written in Go and published under the MIT license.

%prep
mkdir %{name}-%{version}
tar -xzf %{SOURCE0} -C %{name}-%{version}

%build
cd gitea-%{version}
TAGS="bindata pam sqlite sqlite_unlock_notify" GITEA_VERSION="%{version}" make generate build

%install
install -p -D -m 0755 %{_builddir}/gitea-%{version}/gitea %{buildroot}%{_bindir}/gitea
install -p -D -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/gitea.service
install -p -D -m 0664 %{SOURCE2} %{buildroot}%{_sysconfdir}/gitea/app.ini.sample
install -p -D -m 0664 /dev/null  %{buildroot}%{_sysconfdir}/gitea/app.ini
install -p -D -m 0644 %{SOURCE3} %{buildroot}%{_docdir}/gitea/LICENSE
install -p -d -m 0750 %{buildroot}%{_sharedstatedir}/gitea

%pre
if [ $1 == 1 ]; then
    # install
    getent group %{gitea_user} >/dev/null || groupadd -r %{gitea_user}
    getent passwd gitea >/dev/null || \
        useradd -r -g %{gitea_user} -m -s /bin/bash \
        -c "Git Version Control" %{gitea_user}
fi
exit 0

%post
%systemd_post gitea.service

%preun
%systemd_preun gitea.service

%postun
if [ $1 == 0 ]; then
    # uninstall
    if getent passwd %{gitea_user} >/dev/null; then 
        userdel %{gitea_user} 
    fi
    if getent group %{gitea_user} >/dev/null ; then
         groupdel %{gitea_user}
    fi
# else 
    # upgrade
fi
# ehsac
%systemd_postun_with_restart gitea.service

%files
%{_bindir}/gitea
%{_unitdir}/gitea.service
%config(noreplace) %attr(664, root, %{gitea_user}) %{_sysconfdir}/gitea/app.ini.sample
%config(noreplace) %attr(664, root, %{gitea_user}) %{_sysconfdir}/gitea/app.ini
%dir %attr(750, %{gitea_user}, %{gitea_user}) %{_sharedstatedir}/gitea
%doc %{_docdir}/gitea/LICENSE

%changelog
* Tue Jun 09 2020 Aaron Burnett <mullein@adelie.io> 1.11.6
- Version bump to 1.11.6
* Mon May 18 2020 Aaron Burnett <mullein@adelie.io> 1.11.5
- Version bump to 1.11.5
* Tue Mar 10 2020 Aaron Burnett <mullein@adelie.io> 1.11.3
- Version bump to 1.11.3
* Sat Mar 7 2020 Aaron Burnett <mullein@adelie.io> 1.11.2
- Version bump to 1.11.2
* Tue Feb 18 2020 Aaron Burnett <mullein@adelie.io> 1.11.1
- Version bump to 1.11.1
* Mon Jan 20 2020 Aaron Burnett <mullein@adelie.io> 1.10.3
- Version bump to 1.10.3
* Wed Jan 8 2020 Aaron Burnett <mullein@adelie.io> 1.10.2
- Version bump to 1.10.2
* Wed Dec 11 2019 Aaron Burnett <mullein@adelie.io> 1.10.1-2
- Fixes missing build version
- Adds PAM support
* Thu Dec 5 2019 Aaron Burnett <mullein@adelie.io> 1.10.1
- Version bump to 1.10.1
- Bug fixes
* Mon Nov 25 2019 Aaron Burnett <mullein@adelie.io> 1.10.0
- Version bump to 1.10.0
- Fixes systemd service reference to /usr/bin/local/gtea
- Actually builds a gitea binary this time
* Wed Jun 19 2019 Aaron Burnett <mullein@adelie.io> 1.8.3-1
- Version bump to 1.8.3
- First build forked from rchouinard/gitea copr repo

